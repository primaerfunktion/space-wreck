﻿using UnityEngine;
using System.Collections;

public class winScene : MonoBehaviour {
	
	
	public GUIText text;
	public AudioSource explosion;
	public AudioSource talk;
	public ParticleSystem particle;

	public GameObject playerShip;
	public Transform meteor;
	
	void Start () {
		StartCoroutine("Intro");
		talk.Stop();
		talk.volume = 0.1f;
	}
	
	void Update(){

		if(playerShip){
			playerShip.transform.position += new Vector3(0,0,1 * Time.deltaTime);
		}

		if(Input.anyKeyDown){
			StopAllCoroutines();
			levelLoaderScript.Load(0);
		}
	}
	
	IEnumerator Intro(){
		yield return new WaitForSeconds(2);
		talk.Play();
		text.text = "\"Yay! I made it.\"";
		yield return new WaitForSeconds(1.5f);
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(1.0f);

		while(Vector3.Distance(meteor.position,playerShip.transform.position) > 0.1){
			meteor.position = Vector3.MoveTowards(meteor.position,playerShip.transform.position, Time.deltaTime * 30);
			yield return null;
		}

		particle.transform.position = playerShip.transform.position;

		Destroy(meteor.gameObject);
		Destroy (playerShip.gameObject);
		
		//yield return new WaitForSeconds(2f);
		particle.Emit(30);
		
		yield return new WaitForSeconds(0.4f);
		explosion.Play();
		
		yield return new WaitForSeconds(2.5f);
		talk.Play();
		talk.loop = false;
		text.text = "\"...\"";
		yield return new WaitForSeconds(1f);
		text.text = "";
		talk.Stop();

		yield return new WaitForSeconds(2f);

		levelLoaderScript.Load(0);
	}
}
