﻿using UnityEngine;
using System.Collections;

public class playerControllerScript : MonoBehaviour {

	public Transform cam;
	public GUIText info;

	// Update is called once per frame
	void Update () {

		float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * 6;
		float rotationY = Input.GetAxis("Mouse Y") * 6;

		//Mouse Movement

		cam.Rotate(new Vector3(-rotationY,0,0));

		// Body Movement

		transform.localEulerAngles = new Vector3(0,rotationX,0);
		Vector3 input = new Vector3(Input.GetAxis("Horizontal"),0,Input.GetAxis("Vertical"));
		input = transform.TransformDirection(input);

		//transform.position += input * Time.deltaTime * 5;
		rigidbody.velocity = new Vector3(input.x * 5,rigidbody.velocity.y,input.z * 5);

		Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f,0.5f,0));   
		RaycastHit hit;
		if(Physics.Raycast(ray,out hit,1.5f)){
			if(hit.collider.tag == "Usable"){
				info.text = hit.collider.GetComponent<button>().useText;
				if(Input.GetButtonDown("Fire1")){
					hit.collider.SendMessage("Use");
				}
			}else{
				info.text = "";
			}
		}else{
			info.text = "";
		}
	}
}
