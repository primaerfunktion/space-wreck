﻿using UnityEngine;
using System.Collections;

public class shipMovement : MonoBehaviour {

	public Transform earth;
	public Vector3 offset;
	public bool running = true;
	
	// Update is called once per frame
	void Update () {
		Vector3 target = new Vector3(offset.x,offset.y,offset.z);

		if(running){
			SpaceShipController.energy -= (SpaceShipController.speed / 50) * Time.deltaTime;
			earth.transform.position = Vector3.MoveTowards(earth.transform.position,new Vector3(target.x + 100,target.y,target.z), SpaceShipController.speed * Time.deltaTime);
		}
	}
}
