﻿using UnityEngine;
using System.Collections;

public class shipControls : Usables {

	public bool running = true;

	public Transform oxygenBar;
	public Transform energyBar;
	public TextMesh speed;
	
	void Update () {
		oxygenBar.localScale = new Vector3(1,1,SpaceShipController.oxygen / 100);
		energyBar.localScale = new Vector3(1,1,SpaceShipController.energy / 100);

		speed.text = "speed\n" + SpaceShipController.speed.ToString();

		if(running){
			SpaceShipController.energy -= 0.5f * Time.deltaTime;
		}
	}

	void Add(string vector){
		SpaceShipController.speed += 10;

	}
	
	void Subtract(string vector){
		SpaceShipController.speed -= 10;
	}
}
