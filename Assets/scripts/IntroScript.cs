﻿using UnityEngine;
using System.Collections;

public class IntroScript : MonoBehaviour {


	public GUIText text;
	public AudioSource explosion;
	public AudioSource talk;
	public ParticleSystem particle;

	void Start () {
		StartCoroutine("Intro");
		talk.Stop();
	}

	void Update(){
		if(Input.anyKeyDown){
			StopAllCoroutines();
			levelLoaderScript.Load(2);
		}
	}
	
	IEnumerator Intro(){
		yield return new WaitForSeconds(1);
		talk.Play();
		talk.pitch = 1f;
		text.text = "\"I have 200 credits. Give me \nas many spaceships as possible!\"";
		yield return new WaitForSeconds(2.5f);
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(0.3f);
		talk.Play();
		talk.pitch = 0.5f;
		text.text = "\"You only get one! You only need one!\nthe best one!\"";
		yield return new WaitForSeconds(2.5f);
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(0.3f);
		talk.Play();
		talk.pitch = 1.0f;
		text.text = "\"Okay.\"";
		yield return new WaitForSeconds(1.5f);
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(0.3f);
		talk.Play();
		talk.pitch = 0.5f;
		talk.volume = 0.05f;
		text.fontSize = 30;
		text.text = "\"...Nice!\"";
		yield return new WaitForSeconds(1.5f);
		text.color = new Color(1,1,1,1);
		text.fontSize = 60;
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(1f);
		particle.Emit(30);

		yield return new WaitForSeconds(0.4f);
		explosion.Play();

		yield return new WaitForSeconds(2.5f);
		talk.Play();
		talk.loop = false;
		talk.volume = 0.1f;
		talk.pitch = 1.0f;
		text.text = "\"...\"";
		yield return new WaitForSeconds(1f);
		text.text = "";
		talk.Stop();

		yield return new WaitForSeconds(2f);
		levelLoaderScript.Load(2);
	}
}
