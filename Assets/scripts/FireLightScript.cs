﻿using UnityEngine;
using System.Collections;

public class FireLightScript : MonoBehaviour {

	public Light l;
	public bool up = false;

	void Start(){
		l.intensity = 0.3f;
	}
	
	// Update is called once per frame
	void Update () {

		if(up){
			l.intensity += Time.deltaTime / 2;
			if(l.intensity > 0.5) up = false;
		}else{
			l.intensity -= Time.deltaTime / 2;
			if(l.intensity < 0.1) up = true;
		}
	}
}
