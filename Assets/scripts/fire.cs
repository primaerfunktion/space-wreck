﻿using UnityEngine;
using System.Collections;

public class fire : MonoBehaviour {

	public ParticleSystem particle;

	float size = 0;
	bool on = true;
	
	// Update is called once per frame
	void Update () {
		if(on){
			size += Time.deltaTime / 10;

			transform.localScale = new Vector3(size,size,size);

			particle.emissionRate = size * 10;

			SpaceShipController.oxygen -= (size) * Time.deltaTime;
			SpaceShipController.hull -= size * Time.deltaTime;

			if(size <= 0){
				particle.emissionRate = 0;
				Destroy(this.gameObject,5);
				on = false;
				gameObject.tag = "Untagged";
			}
		}
	}

	void Switch(){
		size = -0.1f;
	}
}
