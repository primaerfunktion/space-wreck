﻿using UnityEngine;
using System.Collections;

public class energyLines : MonoBehaviour {

	public LineRenderer line;
	public energyGenerator gen;

	// Update is called once per frame
	void Update () {
		if(gen.running){
			for(int i = 1; i < 5; i++){
				line.SetPosition(i,new Vector3(Random.Range(-0.5f,0.5f),Random.Range(-0.5f,0.5f),Random.Range(-0.5f,0.5f)));
			}
		}else{
			for(int i = 1; i < 5; i++){
				line.SetPosition(i,new Vector3(0,0,0));
			}
		}
	}
}
