﻿using UnityEngine;
using System.Collections;

public class oxygenGenerator : MonoBehaviour {

	public GameObject fire;

	public bool running = true;
	public float energyUse = 10;
	public TextMesh energyUseText;
	public TextMesh switchText;
	public float heat;
	public Transform heatBar;
	Animation anim;

	public ParticleSystem particle;

	void Start(){
		StartCoroutine("Fire");
		anim = GetComponent<Animation>();
	}

	void Update () {
		if(running){
			SpaceShipController.oxygen += (0.15f * energyUse) * Time.deltaTime;
			SpaceShipController.energy -= (0.1f * energyUse) * Time.deltaTime;
			switchText.text = "on";
			heat += energyUse / 2* Time.deltaTime;
			anim.Play();
			particle.Play();
		}else{
			switchText.text = "off";
			anim.Stop();
			particle.Stop ();
		}
		if(SpaceShipController.energy <= 0){
			running = false;
		}
		energyUseText.text = energyUse.ToString();

		heat -= 2f * Time.deltaTime;
		heatBar.localScale = new Vector3(1,1,heat / 100);

		if(heat > 100){
			heat = 100;
		}else if(heat < 0){
			heat = 0;
		}

	}

	void Switch(){
		running = !running;
		print (running);
	}

	void Add(){
		if(energyUse < 10){
			energyUse ++;
		}
	}
	
	void Subtract(){
		if(energyUse > 0){
			energyUse --;
		}
	}

	IEnumerator Fire(){
		while (true){
			yield return new WaitForSeconds(Random.Range(5,20));

			float random = Random.Range(0.0f,100.0f);

			if(random < heat) {
				print ("fire placed");
				running = false;
				GameObject.Instantiate(fire,transform.position,Quaternion.identity);
			}
		}
	}
}
