﻿using UnityEngine;
using System.Collections;

public class energyGenerator : MonoBehaviour {

	public GameObject fire;

	public Transform ball;
	public float energyUse = 10;
	public bool running = true;
	public TextMesh switchText;
	public TextMesh energyUseText;
	public float heat;
	public Transform heatBar;

	void Start(){
		StartCoroutine("Fire");
	}

	void Update () {

		if(running){
			ball.Rotate(new Vector3(0,30 * energyUse * Time.deltaTime,0));
		}

		if(running){
			SpaceShipController.energy += (0.4f * energyUse) * Time.deltaTime;
			switchText.text = "on";
			heat += energyUse / 2* Time.deltaTime;
		}else{
			switchText.text = "off";
		}

		energyUseText.text = energyUse.ToString();

		heat -= 2f * Time.deltaTime;
		heatBar.localScale = new Vector3(1,1,heat / 100);
		
		if(heat > 100){
			heat = 100;
		}else if(heat < 0){
			heat = 0;
		}
	}

	void Switch(){
		running = !running;
	}

	void Add(){
		if(energyUse < 10){
			energyUse ++;
		}
	}

	void Subtract(){
		if(energyUse > 0){
			energyUse --;
		}
	}

	IEnumerator Fire(){
		while (true){
			yield return new WaitForSeconds(Random.Range(5,20));
			
			float random = Random.Range(0.0f,100.0f);
			
			if(random < heat) {
				print ("fire placed");
				running = false;
				GameObject.Instantiate(fire,new Vector3(transform.position.x,transform.position.y + 1,transform.position.z),Quaternion.identity);
			}
		}
	}
}
