﻿using UnityEngine;
using System.Collections;

public class missScript : MonoBehaviour {
	
	
	public GUIText text;
	public AudioSource explosion;
	public AudioSource talk;
	public ParticleSystem particle;
	
	public Transform playerShip;
	public Transform meteor;
	
	void Start () {
		StartCoroutine("Intro");
		talk.Stop();
		talk.volume = 0.1f;
	}
	
	void Update(){
		if(Input.anyKeyDown){
			StopAllCoroutines();
			levelLoaderScript.Load(0);
		}

		playerShip.transform.position += new Vector3(0,0,1 * Time.deltaTime);
	}
	
	IEnumerator Intro(){

		yield return new WaitForSeconds(2);
		talk.Play();
		text.text = "\"Dammit.\"";
		yield return new WaitForSeconds(1.5f);
		talk.Stop();
		text.text = "";
	
		yield return new WaitForSeconds(2.5f);
		talk.Play();
		talk.loop = false;
		text.text = "\"...\"";
		yield return new WaitForSeconds(2f);
		talk.Stop();
		text.text = "";

		yield return new WaitForSeconds(2f);
		
		levelLoaderScript.Load(0);
	}
}
