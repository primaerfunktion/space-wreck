﻿using UnityEngine;
using System.Collections;

public class steeringContainer : MonoBehaviour {

	public shipMovement shipM;
	public Vector3 shipTarget;
	public Transform earth;

	public TextMesh targetText;
	public TextMesh distanceText;
	public TextMesh hullText;

	// Update is called once per frame
	void Update () {
		shipTarget = shipM.offset;
		hullText.text = "Hull: " + Mathf.Round(SpaceShipController.hull) + "%";
		distanceText.text = Mathf.Round(Vector3.Distance(Vector3.zero,earth.position)).ToString();
		targetText.text = shipTarget.y + "," + shipTarget.z;
	}

	void Add(string vector){
		switch (vector){
		case "y":
			shipM.offset.y += 10;
			break;
		case "z":
			shipM.offset.z += 10;
			break;
		}
	}

	void Subtract(string vector){
		switch (vector){
		case "y":
			shipM.offset.y -= 10;
			break;
		case "z":
			shipM.offset.z -= 10;
			break;
		}
	}
}
