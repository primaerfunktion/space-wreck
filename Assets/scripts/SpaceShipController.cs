﻿using UnityEngine;
using System.Collections;

public class SpaceShipController : MonoBehaviour {

	public static float speed = 30;
	public static float oxygen = 100;
	public static float energy = 100;
	public static float hull = 83;

	public shipMovement move;

	bool destroyed;

	void Start(){
		StartCoroutine("RandomEvents");
	}

	void Update(){

		if(oxygen <= 0){
			oxygen = 0;
		}else if(oxygen >= 100.01){
			oxygen = 100;
		}

		if(energy <= 0){
			energy = 0;
			speed = 0;
		}else if(energy >= 100.01){
			energy = 100;
		}

		if(hull <= 0 && !destroyed){
			levelLoaderScript.Load(5);
			destroyed = true;
		}
	}

	IEnumerator RandomEvents(){
		while (true){
			yield return new WaitForSeconds(Random.Range(10,30));
			if(Random.Range(0.0f,1.0f) > 0.3f){
				print ("offset");
				move.offset += new Vector3(0,Random.Range(-10,10),Random.Range(-10,10) * 10);
			}
		}
	}
}
