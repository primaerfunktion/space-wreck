﻿using UnityEngine;
using System.Collections;

public class button : MonoBehaviour {
		
	public enum Type{
		switcher,
		adder,
		subtracter
	}

	public Type type;
	public string useText = "null";

	public string vector;

	public GameObject target;

	void Use(){
		switch(type){
		case Type.switcher:
			target.SendMessage("Switch");
			break;
		case Type.adder:
			target.SendMessage("Add",vector);
			break;
		case Type.subtracter:
			target.SendMessage("Subtract",vector);
			break;
		}
	}
}
