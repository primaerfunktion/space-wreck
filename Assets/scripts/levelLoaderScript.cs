﻿using UnityEngine;
using System.Collections;

public class levelLoaderScript : MonoBehaviour {

	public GUITexture texture;

	static int levelToLoad;
	public static bool load = false;
	public static bool start = false;

	// Use this for initialization
	void Awake () {
		texture.pixelInset = new Rect(0,0,Screen.width,Screen.height);
		texture.color = new Color(0,0,0,1);
		Screen.showCursor = false;
		start = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(start){
			texture.color = Color.Lerp(texture.color,new Color(0,0,0,0), Time.deltaTime * 2);
			if(texture.color.a < 0.01){
				texture.color = new Color(0,0,0,0);
				start = false;
			}
		}

		if(load){
			start = false;
			texture.color = Color.Lerp(texture.color,new Color(0,0,0,1), Time.deltaTime * 2);
			if(texture.color.a > 0.9){
				texture.color = new Color(0,0,0,1);
				load = false;
				Application.LoadLevel(levelToLoad);
			}
		}
	}

	public static void Load(int level){
		levelToLoad = level;
		load = true;
	}
}
