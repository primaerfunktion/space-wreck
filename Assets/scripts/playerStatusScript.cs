﻿using UnityEngine;
using System.Collections;

public class playerStatusScript : MonoBehaviour {

	public float health = 100;
	bool dead;
	public GUITexture healthBar;

	void Update(){

		SpaceShipController.oxygen -= 0.8f * Time.deltaTime;

		if(SpaceShipController.oxygen <= 20){
			health -= 1f * Time.deltaTime;
		}

		if(health < 0 && !dead){
			health = 0;
			levelLoaderScript.Load(5);
			dead = true;
		}

		healthBar.pixelInset = new Rect(healthBar.pixelInset.x,healthBar.pixelInset.y,health * 2,healthBar.pixelInset.height);
	}
}
