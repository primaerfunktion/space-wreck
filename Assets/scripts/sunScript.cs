﻿using UnityEngine;
using System.Collections;

public class sunScript : MonoBehaviour {

	public shipMovement ship;

	void Start(){
		transform.LookAt(new Vector3(ship.offset.x,ship.offset.y - 23,ship.offset.z + 6));
	}

	void Update () {
		Quaternion rotation = Quaternion.LookRotation(new Vector3(ship.offset.x,ship.offset.y - 23,ship.offset.z + 6) - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime / 2);
	}
}
